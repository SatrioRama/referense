<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>

@if (backpack_user()->hasRole('admin'))
<!-- Users, Roles, Permissions -->
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Authentication</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Roles</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a></li>
    </ul>
</li>
@endif

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('book') }}'><i class='nav-icon la la-question'></i> Books</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('inbound') }}'><i class='nav-icon la la-question'></i> Inbounds</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('stock') }}'><i class='nav-icon la la-question'></i> Stocks</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('outbound') }}'><i class='nav-icon la la-question'></i> Outbounds</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('loan') }}'><i class='nav-icon la la-question'></i> Loans</a></li>