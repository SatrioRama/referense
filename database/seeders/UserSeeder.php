<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::create([
            'name' => 'Satrio Rama',
            'username' => 'satrio',
            'email' => 'satrio.rama@trilogi.ac.id',
            'password' => bcrypt('password')
        ]);

        $user1->assignRole('admin');

        $user2 = User::create([
            'name' => 'Staff',
            'username' => 'staff',
            'email' => 'staff@trilogi.ac.id',
            'password' => bcrypt('password')
        ]);

        $user2->assignRole('staff');

        $user3 = User::create([
            'name' => 'User',
            'username' => 'user',
            'email' => 'user@trilogi.ac.id',
            'password' => bcrypt('password')
        ]);

        $user3->assignRole('user');
    }
}
