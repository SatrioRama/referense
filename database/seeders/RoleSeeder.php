<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $admin = Role::create([
            'name' => 'admin',
        ]);

        $staff = Role::create([
            'name' => 'staff',
        ]);

        $user = Role::create([
            'name' => 'user',
        ]);
    }
}
