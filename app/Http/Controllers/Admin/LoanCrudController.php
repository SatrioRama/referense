<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LoanRequest;
use App\Models\Loan;
use App\Models\Stock;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class LoanCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LoanCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Loan::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/loan');
        CRUD::setEntityNameStrings('loan', 'loans');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('book_id');
        CRUD::column('loan_date');
        CRUD::column('return_date');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(LoanRequest::class);

        CRUD::field('book_id');
        CRUD::field('loan_date');
        CRUD::field('return_date');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store(Request $request)
    {
        /*
        |--------------------------------------------------------------------------
        | VALIDATION
        |--------------------------------------------------------------------------
        */
        $rules_request = new LoanRequest();
        $this->validate($request, $rules_request->rules(), $rules_request->messages());

        /*
        |--------------------------------------------------------------------------
        | STORE DATA
        |--------------------------------------------------------------------------
        */
        //loan Entity
        $save = new Loan();
        $save->book_id = $request->book_id;
        $save->loan_date = $request->loan_date;
        $save->return_date = $request->return_date;
        $save->save();

        //Stock Entity
        $find_stock = Stock::where('book_id', '=', $request->book_id)->first();
        $stock = Stock::find($find_stock->id);
        $stock->qty -= 1;
        $stock->on_loan += 1;
        $stock->update();

        /*
        |--------------------------------------------------------------------------
        | PAGE RETURN
        |--------------------------------------------------------------------------
        */
        \Alert::success('Data berhasil ditambahkan')->flash();
        if ($request->save_action == 'save_and_back') {
            return redirect($request->http_referrer);
        }elseif ($request->save_action == 'save_and_new') {
            return redirect(backpack_url('loan/create'));
        }else {
            return redirect(backpack_url('loan'));
        }

    }

    public function destroy($id)
    {
        //restore stock
        $find_last = Loan::find($id);
        $check_stock = Stock::where('book_id', '=', $find_last->book_id)->first();
        $stock = Stock::find($check_stock->id);
        $stock->qty += 1;
        $stock->on_loan -= 1;
        $stock->update();

        $id = $this->crud->getCurrentEntryId() ?? $id;

        return $this->crud->delete($id);
    }
}
