<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\InboundRequest;
use App\Models\Inbound;
use App\Models\Stock;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class InboundCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class InboundCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Inbound::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/inbound');
        CRUD::setEntityNameStrings('inbound', 'inbounds');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            // any type of relationship
            'name'         => 'book', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Book', // Table column heading
            // OPTIONAL
            // 'entity'    => 'tags', // the method that defines the relationship in your Model
            'attribute' => 'title', // foreign key attribute that is shown to user
            'model'     => App\Models\Book::class, // foreign key model
         ]);

         $this->crud->addColumn([
             'name' => 'qty',
             'Label'=> 'Qty',
             'type' => 'Number'
         ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(InboundRequest::class);

        $this->crud->addField([
            'type' => "relationship",
            'name' => 'book_id', // the method on your model that defines the relationship
            'entity'    => 'book',
            'attribute' => 'title',
            'model'     => 'App\Models\Book',
            'ajax' => false,
            'inline_create' => true, // assumes the URL will be "/admin/category/inline/create"
        ]);

        $this->crud->addField([   // Number
            'name' => 'qty',
            'label' => 'Qty',
            'type' => 'number',
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    protected function fetchBook()
    {
        return $this->fetch([
            'model' => \App\Models\Book::class, // required
            'searchable_attributes' => ['title'],
        ]);
    }



    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store(Request $request)
    {
        /*
        |--------------------------------------------------------------------------
        | VALIDATION
        |--------------------------------------------------------------------------
        */
        $rules_request = new InboundRequest;
        $this->validate($request, $rules_request->rules(), $rules_request->messages());

        /*
        |--------------------------------------------------------------------------
        | STORE DATA
        |--------------------------------------------------------------------------
        */
        //Inbound Entity
        $save = new Inbound();
        $save->book_id = $request->book_id;
        $save->qty = $request->qty;
        $save->save();

        //Stock Entity
        $find_stock = Stock::where('book_id', '=', $request->book_id)->first();
        if (empty($find_stock)) { //New Stock
            $stock = new Stock();
            $stock->book_id = $request->book_id;
            $stock->qty = $request->qty;
            $stock->save();
        } else {
            $stock = Stock::find($find_stock->id);
            $stock->qty += $request->qty;
            $stock->update();
        }

        /*
        |--------------------------------------------------------------------------
        | PAGE RETURN
        |--------------------------------------------------------------------------
        */
        \Alert::success('Data berhasil ditambahkan')->flash();
        if ($request->save_action == 'save_and_back') {
            return redirect($request->http_referrer);
        }elseif ($request->save_action == 'save_and_new') {
            return redirect(backpack_url('inbound/create'));
        }else {
            return redirect(backpack_url('inbound'));
        }

    }

    public function update(Request $request, $id)
    {
        /*
        |--------------------------------------------------------------------------
        | VALIDATION
        |--------------------------------------------------------------------------
        */
        $rules_request = new InboundRequest;
        $this->validate($request, $rules_request->rules(), $rules_request->messages());

        /*
        |--------------------------------------------------------------------------
        | STORE DATA
        |--------------------------------------------------------------------------
        */
        //restore stock
        $find_last = Inbound::find($id);
        $check_stock = Stock::where('book_id', '=', $find_last->book_id)->first();
        $update_stock = Stock::find($check_stock->id);
        $update_stock->qty -= $find_last->qty;
        $update_stock->update();

        //Inbound Entity
        $update = Inbound::find($id);
        $update->book_id = $request->book_id;
        $update->qty = $request->qty;
        $update->update();

        //Stock Entity
        $find_stock = Stock::where('book_id', '=', $request->book_id)->first();
        if (empty($find_stock)) { //New Stock
            $stock = new Stock();
            $stock->book_id = $request->book_id;
            $stock->qty = $request->qty;
            $stock->save();
        } else {
            $stock = Stock::find($find_stock->id);
            $stock->qty += $request->qty;
            $stock->update();
        }

        /*
        |--------------------------------------------------------------------------
        | PAGE RETURN
        |--------------------------------------------------------------------------
        */
        \Alert::success('Data berhasil ditambahkan')->flash();
        if ($request->save_action == 'save_and_back') {
            return redirect($request->http_referrer);
        }elseif ($request->save_action == 'save_and_new') {
            return redirect(backpack_url('inbound/create'));
        }else {
            return redirect(backpack_url('inbound'));
        }

    }

    public function destroy($id)
    {
        //restore stock
        $find_last = Inbound::find($id);
        $check_stock = Stock::where('book_id', '=', $find_last->book_id)->first();
        $update_stock = Stock::find($check_stock->id);
        $update_stock->qty -= $find_last->qty;
        $update_stock->update();

        $id = $this->crud->getCurrentEntryId() ?? $id;

        return $this->crud->delete($id);
    }
}
