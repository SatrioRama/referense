<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OutboundRequest;
use App\Models\Outbound;
use App\Models\Stock;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class OutboundCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OutboundCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Outbound::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/outbound');
        CRUD::setEntityNameStrings('outbound', 'outbounds');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('book_id');
        CRUD::column('qty');
        CRUD::column('description');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(OutboundRequest::class);

        $this->crud->addFields(['book_id', 'qty', 'description']);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store(Request $request)
    {
        /*
        |--------------------------------------------------------------------------
        | VALIDATION
        |--------------------------------------------------------------------------
        */
        $rules_request = new OutboundRequest();
        $this->validate($request, $rules_request->rules(), $rules_request->messages());

        /*
        |--------------------------------------------------------------------------
        | STORE DATA
        |--------------------------------------------------------------------------
        */
        //outbound Entity
        $save = new Outbound();
        $save->book_id = $request->book_id;
        $save->qty = $request->qty;
        $save->description = $request->description;
        $save->save();

        //Stock Entity
        $find_stock = Stock::where('book_id', '=', $request->book_id)->first();

        $stock = Stock::find($find_stock->id);
        $stock->qty -= $request->qty;
        $stock->update();

        /*
        |--------------------------------------------------------------------------
        | PAGE RETURN
        |--------------------------------------------------------------------------
        */
        \Alert::success('Data berhasil ditambahkan')->flash();
        if ($request->save_action == 'save_and_back') {
            return redirect($request->http_referrer);
        }elseif ($request->save_action == 'save_and_new') {
            return redirect(backpack_url('outbound/create'));
        }else {
            return redirect(backpack_url('outbound'));
        }

    }

    public function update(Request $request, $id)
    {
        /*
        |--------------------------------------------------------------------------
        | VALIDATION
        |--------------------------------------------------------------------------
        */
        $rules_request = new OutboundRequest();
        $this->validate($request, $rules_request->rules(), $rules_request->messages());

        /*
        |--------------------------------------------------------------------------
        | STORE DATA
        |--------------------------------------------------------------------------
        */
        //restore stock
        $find_last = Outbound::find($id);
        $check_stock = Stock::where('book_id', '=', $find_last->book_id)->first();
        $update_stock = Stock::find($check_stock->id);
        $update_stock->qty += $find_last->qty;
        $update_stock->update();

        //outbound Entity
        $update = Outbound::find($id);
        $update->book_id = $request->book_id;
        $update->qty = $request->qty;
        $update->update();

        //Stock Entity
        $find_stock = Stock::where('book_id', '=', $request->book_id)->first();

        $stock = Stock::find($find_stock->id);
        $stock->qty -= $request->qty;
        $stock->update();

        /*
        |--------------------------------------------------------------------------
        | PAGE RETURN
        |--------------------------------------------------------------------------
        */
        \Alert::success('Data berhasil ditambahkan')->flash();
        if ($request->save_action == 'save_and_back') {
            return redirect($request->http_referrer);
        }elseif ($request->save_action == 'save_and_new') {
            return redirect(backpack_url('outbound/create'));
        }else {
            return redirect(backpack_url('outbound'));
        }

    }

    public function destroy($id)
    {
        //restore stock
        $find_last = Outbound::find($id);
        $check_stock = Stock::where('book_id', '=', $find_last->book_id)->first();
        $update_stock = Stock::find($check_stock->id);
        $update_stock->qty += $find_last->qty;
        $update_stock->update();

        $id = $this->crud->getCurrentEntryId() ?? $id;

        return $this->crud->delete($id);
    }
}
